package Noyau;

public class Multiplication extends OperationBinaire{
	private static final long serialVersionUID = 1L;
	//Constructeur
	public Multiplication(Case g, Case d){
		super(g,d);
	}
	//Methodes
	public String getOperateur(){//Retourne le signe de l'opération 
		return "*";
	}
	public double eval() {//Retourne l'évalutation de l'opération 
		if (!Double.isNaN(droite.valeur()) && !Double.isNaN(gauche.valeur()))
			return gauche().valeur()*droite().valeur();
		// Pas de valeur 
		return Double.NaN;
	}
}