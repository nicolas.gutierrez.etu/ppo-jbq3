package Noyau;

import java.util.LinkedList;

public class Somme extends Fonction{
	private static final long serialVersionUID = 1L;
	//Constructeur
	public Somme(LinkedList<Case> Cases){
		super(Cases);
	}
	
	//Methodes
	public String getOperateur(){//Retourne l'opérateur de l'operation
		return "SOMME";
	}
	
	public double eval() {//Retourne l'évalutation de l'opération 
		double Sum=0;
		int i=0;
		while (i<CasesDep.size() && !Double.isNaN(CasesDep.get(i).valeur())) { //On parcours les cases de la liste
			Sum+=CasesDep.get(i).valeur();
			i++;
		}
		if (i==CasesDep.size())
			return Sum;
		else
			return Double.NaN;
	}
}
