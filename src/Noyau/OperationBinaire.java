package Noyau;

import java.util.LinkedList;

public abstract class OperationBinaire extends Formule{
	private static final long serialVersionUID = 1L;
	protected Case gauche;//Case a gauche de l'operateur
	protected Case droite;//Case a droite de l'operateur
	
	//getters
	public Case gauche() {
		return gauche;
	}
	public Case droite() {
		return droite;
	}
	
	//setters
	public void setGauche(Case c) {
		gauche =c;
	}
	public void setDroite(Case c) {
		droite =c;
	}
	
	//Constructeur
	public OperationBinaire(Case g, Case d){
		setGauche(g);
		setDroite(d);
	}
	
	//methodes
	public LinkedList<Case> getCellules(){//Retourne la liste des cases
		LinkedList<Case> cells= new LinkedList<Case>();
		cells.add(gauche());
		cells.add(droite);
		return cells;
	}
	
	public String toString() {//Affichage de l'operation 
		return gauche().getCoord()+getOperateur()+droite().getCoord();
	}
	
}
