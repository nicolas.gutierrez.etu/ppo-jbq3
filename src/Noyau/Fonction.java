package Noyau;
import java.util.LinkedList;

public abstract class Fonction extends Formule{
	private static final long serialVersionUID = 1L;
	protected LinkedList<Case> CasesDep = new LinkedList<Case>(); //Liste des cases utilisées dans la fonction
	//Constructeur
	
	public Fonction(LinkedList<Case> Cases){
		CasesDep=Cases;
	}
	
	//Methodes
	
	public LinkedList<Case> getCellules(){//Retourne la liste des cases utilisées dans la fonction
		return this.CasesDep; 
	}
	
	public String toString() //Affichage de la fonction 
	{
		LinkedList<Case> cells = getCellules();
		String fonct = getOperateur()+"(";
		for (int i=0;i<cells.size()-1;i++) //On parcours les cases utilisées par la fonction
			fonct+= cells.get(i).getCoord()+";"; //On ajoute les cases séparées par des ;
		fonct+=cells.getLast().getCoord()+")";
		return fonct;
	}
}
