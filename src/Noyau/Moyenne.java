package Noyau;

import java.util.LinkedList;

public class Moyenne extends Fonction{
	private static final long serialVersionUID = 1L;
	//Constructeur

	public Moyenne(LinkedList<Case> Cases){
		super(Cases);
	}
	
	//Methodes
	public String getOperateur(){//Retourne l'opérateur de l'operation
		return "MOYENNE";
	}
	
	public double eval(){//Retourne l'évalutation de l'opération 
		if (CasesDep.size()!=0)
		{
			double Sum=0;
			int i=0;
			while(i<CasesDep.size() && !Double.isNaN(CasesDep.get(i).valeur())){
				Sum+=CasesDep.get(i).valeur();
				i++; //On parcours les cases de la liste
			}
			if (i==CasesDep.size()){
				return Sum/CasesDep.size();
			}
			else{
				return Double.NaN;
			}
		}
		else {
			return Double.NaN;
		}
	}
}
