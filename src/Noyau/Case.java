package Noyau;
import java.io.Serializable;
import java.util.LinkedList;

public class Case implements Serializable{
	
	private static final long serialVersionUID = 1L;// Warning Eclipse
	protected String colonne;//nom de la colonne contenant la case
	protected int ligne;//numero de ligne contenant la case
	protected double valeur;//valeur de la case
	protected Formule formule;//formule de la case, si elle existe
	protected LinkedList<Case> estUtilisePar = new LinkedList<Case>(); //Liste des cases qui dépendent de la case courante

	
	//Getters
	public String colonne(){
		return colonne;
	}
	public int ligne(){
		return ligne;
	}
	public double valeur(){
		return this.valeur;
	}
	public Formule formule() throws FormuleInconnue{
		if (formule== null)
			throw new FormuleInconnue();
		return formule;
	}
	public LinkedList<Case> getEstUtilisePar(){
		return (LinkedList<Case>)estUtilisePar; //Retourne la liste des cases qui dépendent de la case courante
	}
	
	//Setters
	public void fixerColonne(String c){
		colonne=c;
	}
	public void fixerLigne(int l){
		ligne=l;
	}
	public void fixerValeur(double valeur){
		this.valeur=valeur;
		propagation();
	}
	public void setFormule(Formule f) throws CycleDetected{
		if (f.detectCycle(this.getCoord())){
				throw new CycleDetected();
			}
		else{
			formule=f;
			LinkedList<Case> cells = f.getCellules();
			fixerValeur(f.eval());
			for (Case c:cells)
				c.addCaseDep(this);
		}
	}
	
	//Constructeurs
	public Case(String col, int lig, double val){// Si pas de formule
		fixerColonne(col);
		fixerLigne(lig);
		fixerValeur(val);
	}
	public Case(String col, int lig, Formule form) throws CycleDetected{// Si formule
		fixerColonne(col);
		fixerLigne(lig);
		setFormule(form);
	}
	
	//Méthodes
	public String getCoord() {// retourne les coordonnees de la case
		return colonne()+ligne();
	}
	
	public void propagation(){//cycle de propagation lors de la modification d'une case
			for (Case c: estUtilisePar) {
			try{
				c.fixerValeur(c.formule().eval());
			}
			catch (FormuleInconnue e) {}// Sur une case dépendante, il y aura toujours une formule.
		}
	}
	
	public void addCaseDep(Case c) {//ajouter une case à la liste de cases dépendantes
		estUtilisePar.add(c);
	}
	
	public String getFormuleDev() throws FormuleInconnue{ // Retourne la formule développée de la case courante
		try { //S'il y a une formule dans la case (condition utile dans la récurrence)
			LinkedList <Case> cells = formule().getCellules(); //On récupère la liste chainée contenant les cases utilisées dans la formule
			String f; //On initialise la chaine de caractères qui contiendra la formule développée
			if (formule() instanceof Fonction) { //S'il y a une fonction (MOYENNE ou SOMME) dans la case
				f= formule().getOperateur()+"(";
				for (int i=0;i<cells.size()-1;i++) //On parcours les éléments de la liste
					f+= cells.get(i).getFormuleDev()+";"; //Appel récurrent de getFormuleDev (tant que les cases ont des formules)
				f+=cells.getLast().getFormuleDev()+")";
			}
			else { //Si la formule est à base d'opération binaire
				f= cells.get(0).getFormuleDev();
				for (int i=1;i<cells.size();i++) //On parcours les éléments de la liste
					f+= formule().getOperateur()+cells.get(i).getFormuleDev();
			}
			return f;
		}
		catch (FormuleInconnue e) { //S'il n'y a pas de formule dans la case: On écrit juste les coordonnées de la case
			return getCoord();
		}
	}
	
	public String getContenu(){//Retourne le contenu de la case
		String contenu = "\t>Valeur : "+ valeur();
		try {
			contenu+= "\n\t>Contenu : "+ formule().toString()+
					  "\n\t>Contenu developpé : "+ getFormuleDev();
		}
		catch (FormuleInconnue e) {}
		
		return contenu;
	}
}
