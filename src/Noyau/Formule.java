package Noyau;
import java.io.Serializable;
import java.util.LinkedList;

public abstract class Formule implements Serializable{
	private static final long serialVersionUID = 1L;

	public abstract double eval(); //Retourne l'evaluation de la formule
	public abstract LinkedList<Case> getCellules(); //Retourne la listedes cellules utilisées dans la formule
	public abstract String getOperateur();//Retourne l'opérateur de la formule
	
	public boolean detectCycle(String Origin){ //Méthode permettant de détecter un cycle lors de la modification d'une formule
		boolean test=false; //Tant que test est à false, il n'y a pas de cycle
		int i=0;
		LinkedList<Case> cells=this.getCellules();
		while (i<cells.size() && !test) { //On cherche un cycle tant qu'il existe des cases non visitées et tant qu'on a pas trouvé de cycle
			if (Origin.equals(cells.get(i).getCoord())) {
				test=true;
			}
			else {
				try {
					test = cells.get(i).formule().detectCycle(Origin); //Appel récurrent de detectCycle pour chaque case
				}
				catch (FormuleInconnue e) {}
			}
			i++;
		}
		return test;
	}
}
