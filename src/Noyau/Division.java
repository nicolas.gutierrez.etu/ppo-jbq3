package Noyau;

public class Division extends OperationBinaire{
	private static final long serialVersionUID = 1L;
	//Constructeur
	public Division(Case g, Case d){
		super(g,d);
	}
	//Methodes
	public String getOperateur(){//Retourne le signe de l'opération 
		return "/";
	}
	public double eval(){//Retourne l'évalutation de l'opération 
		if (!Double.isNaN(droite.valeur()) && !Double.isNaN(gauche.valeur()) && droite().valeur()!=0) {
			return gauche().valeur()/droite().valeur();	
		}
		else {
			return Double.NaN;
		}
	}
}