package Noyau;
import java.io.*;
import java.util.TreeMap;

public class Grille {
	int nbL;//Nombre de lignes de la grille
	int nbC;//Nombre de colonnes de la grille
	TreeMap<String,Case> cases; //Tri des cases par coordonnées
	//constructeur
	public Grille(){
		nbL=20;
		nbC=26;
		cases= new TreeMap<String,Case>();
	}
	public Grille(int l, int c){// choix des dimensions de la grille
		nbL=l;
		nbC=c;
		cases= new TreeMap<String,Case>();
	}
	//getters
	public int nbLigne(){
		return this.nbL;
	}
	
	public int nbColonne(){
		return this.nbC;
	}	
	//methodes
	public double getValeur(String coord) {//Retourne la valeur d'une case, 0 si case vide
		try {
			return getCase(coord).valeur();
		}
		catch (CaseInconnueException e) {
			return 0;// Si la case est vide, elle vaut par défaut 0.
		}
	}
	public Case getCase(String coord) throws CaseInconnueException{//Retourne la case
		Case c=cases.get(coord);
		if (c==null)
			throw new CaseInconnueException();
		return c;
	}
	public void addCase(Case c) {//Ajout d'une case
		cases.put(c.getCoord(),c);// TEST : Cases de même coordonnées
	}
	public void removeCase(String coord){//Suppression d'une case
		cases.remove(coord);
	}
	//Sauvegarde de la grille
	public void save (String backupName) throws IOException{
		ObjectOutputStream out = 
				new ObjectOutputStream(new FileOutputStream(backupName)); 
		
		out.writeObject(cases);
		out.close();
	}
	//Chargement de la grille
	@SuppressWarnings("unchecked")// Warning Eclipse
	public void load(String backupName) 
			throws IOException,ClassNotFoundException{
		ObjectInputStream in = 
				new ObjectInputStream(new FileInputStream(backupName)); 
		
		cases= (TreeMap<String,Case>)in.readObject();
		in.close();
	}
	
}
