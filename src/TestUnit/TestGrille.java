package TestUnit;

import org.junit.*;
import Noyau.*;

public class TestGrille {
	Grille grille;
	Case caseA1;
	Case caseB5;
	@Before public void setUp(){
		grille = new Grille();
		caseA1 = new Case("A",1,12);
		caseB5 = new Case("B",5,24);
	}
	/*GET*/
	@Test
	public void TestGetValeurCase() {
		grille.addCase(caseA1);
		Assert.assertEquals(12, grille.getValeur("A1"), 0.0);
	}
	//Cas où la case est vide
	@Test(expected=CaseInconnueException.class)
	public void TestGetCaseInconnue() throws CaseInconnueException{
		grille.getCase("A1");
	}
	@Test public void TestGetValeurCaseInconnue() {
		Assert.assertEquals(0, grille.getValeur("A1"), 0.0);
	}	
	/*ADD*/
	@Test
	public void testaddCase() throws CaseInconnueException{
		grille.addCase(caseA1);
		Assert.assertEquals(caseA1.getContenu(), grille.getCase(caseA1.getCoord()).getContenu());
	}
	/*REMOVE + Get de la case removed: Case Inconnue*/
	@Test (expected=CaseInconnueException.class)
	public void testremoveCase() throws CaseInconnueException{
		grille.removeCase(caseB5.getCoord());
		grille.getCase(caseB5.getCoord());
	}	
}
