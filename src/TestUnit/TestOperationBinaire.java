package TestUnit;

import Noyau.*;
import org.junit.*;

public class TestOperationBinaire {

	@Test
	public void testAddition(){
		Case g = new Case("A",1,3);
		Case d = new Case("A",2,3);
		Addition a = new Addition(g,d);
		Assert.assertEquals(6, a.eval(), 0.0);
	}
	@Test
	public void testSoustraction(){
		Case g = new Case("A",1,3);
		Case d = new Case("A",2,3);
		Soustraction a = new Soustraction(g,d);
		Assert.assertEquals(0, a.eval(), 0.0);
	}
	@Test
	public void testMultiplication(){
		Case g = new Case("A",1,3);
		Case d = new Case("A",2,3);
		Multiplication a = new Multiplication(g,d);
		Assert.assertEquals(9, a.eval(), 0.0);
	}
	@Test
	public void testDivision(){
		Case g = new Case("A",1,3);
		Case d = new Case("A",2,3);
		Division a = new Division(g,d);
		Assert.assertEquals(1, a.eval(), 0.0);
	}
	@Test
	public void testNaN(){
		Case g = new Case("A",1,3);
		Case d = new Case("A",2,Double.NaN);
		Division a = new Division(g,d);
		Assert.assertEquals(Double.NaN, a.eval(), 0.0);
	}

}
