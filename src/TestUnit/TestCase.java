package TestUnit;

import java.util.LinkedList;

import org.junit.*; //A toujours faire
import Noyau.*;

public class TestCase {
	Case c1,c2, c3, c4;
	@Before public void setUp() throws CycleDetected{
		LinkedList<Case>l=new LinkedList<Case>();
		c1=new Case("A",1, 32);
		c2=new Case("A",2, 30);
		c3=new Case("A",3, new Addition(c1,c2));
		l.add(c1);
		l.add(c2);
		l.add(c3);
		c4=new Case("B",1, new Somme(l));
	}
	@Test
	public void testValeur(){
		Assert.assertEquals(32, c1.valeur(), 0.0);
	}
	@Test
	public void testCoord(){
		Assert.assertEquals("A1", c1.getCoord());
	}
	@Test
	public void testColonne(){
		Assert.assertEquals("A", c1.colonne());
	}
	@Test
	public void testLigne(){
		Assert.assertEquals(1, c1.ligne(), 0.0);
	}
	@Test
	public void testContenu(){
		Assert.assertEquals("\t>Valeur : 32.0", c1.getContenu());
	}
	// With Formulas
	@Test
	public void testFormule() throws FormuleInconnue{
		Assert.assertEquals("A1+A2", c3.formule().toString());
	}
	@Test
	public void testValeurFormule() throws FormuleInconnue{
		Assert.assertEquals(62.0, c3.valeur(),0.0);
	}
	@Test// Operation Binaire
	public void testContenuWithAddition(){
		Assert.assertEquals("\t>Valeur : 62.0\n\t>Contenu : A1+A2\n\t>Contenu developpé : A1+A2", c3.getContenu());
	}
	@Test// Fonction
	public void testContenuWithSomme(){
		Assert.assertEquals("\t>Valeur : 124.0\n\t>Contenu : SOMME(A1;A2;A3)\n\t>Contenu developpé : SOMME(A1;A2;A1+A2)", c4.getContenu());
	}
	@Test// Propagation
	public void testPropagation(){
		c1.fixerValeur(0);
		Assert.assertEquals("\t>Valeur : 60.0\n\t>Contenu : SOMME(A1;A2;A3)\n\t>Contenu developpé : SOMME(A1;A2;A1+A2)", c4.getContenu());
	}
}
