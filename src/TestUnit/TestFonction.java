package TestUnit;
import Noyau.*;

import org.junit.*; //A toujours faire

import java.util.LinkedList;

import org.junit.Test;

public class TestFonction {

	@Test //procedure de test 1: somme
	public void testSomme()
	{
		// situation
		LinkedList<Case> Cases = new LinkedList<Case>();
		Case caseA1 = new Case("A",1,12);
		Cases.add(caseA1);
		Case caseA2 = new Case("A",2,11);
		Cases.add(caseA2);
		Case caseA3 = new Case("A",3,10);
		Cases.add(caseA3);
		Case caseA4 = new Case("A",4,9);
		Cases.add(caseA4);
		Case caseA5 = new Case("A",5,8);
		Cases.add(caseA5);		
		Somme somme = new Somme(Cases);
		// action
		double Sum=somme.eval();
		// oracle
		Assert.assertEquals(50.0, Sum, 0.0);
	}
	
	@Test //procedure de test 2: moyenne
	public void testMoyenne()
	{
		// situation
		LinkedList<Case> Cases = new LinkedList<Case>();
		Case caseA1 = new Case("A",1,12);
		Cases.add(caseA1);
		Case caseA2 = new Case("A",2,11);
		Cases.add(caseA2);
		Case caseA3 = new Case("A",3,10);
		Cases.add(caseA3);
		Case caseA4 = new Case("A",4,9);
		Cases.add(caseA4);
		Case caseA5 = new Case("A",5,8);
		Cases.add(caseA5);		
		Moyenne moyenne = new Moyenne(Cases);
		// action
		double Moy=moyenne.eval();
		// oracle
		Assert.assertEquals(10.0, Moy, 0.0);
	}
	
	@Test //procedure de test 3: moyenne sur 0 éléments
	public void Moyenne0element()
	{
		// situation
		LinkedList<Case> Cases = new LinkedList<Case>();
		Moyenne moyenne = new Moyenne(Cases); //moyenne sur une liste vide
		// action
		
		Assert.assertEquals(Double.NaN, moyenne.eval(),0.0);
	}

}
