package TestUnit;

import Noyau.*;

import java.io.IOException;
import java.util.LinkedList;

import org.junit.*;

public class initGrille {
	Grille initgrille;
	Case caseA1;
	Case caseA2,caseB2,caseC2;
	Case caseA3;
	Case caseA4,caseB4,caseC4,caseE4;
	Case caseA6,caseB6,caseC6,caseD6;
	LinkedList<Case> CasesB4_E4 = new LinkedList<Case>();
	LinkedList<Case> CasesA2_B2_A3 = new LinkedList<Case>();
	LinkedList<Case> CasesB6_B4_A4 = new LinkedList<Case>();
	@Before public void setUp() throws CycleDetected{
		initgrille = new Grille();
		caseA1 = new Case("A",1,100);
		caseA2 = new Case("A",2,50);
		caseB2 = new Case("B",2,12);
		caseC2 = new Case("C",2,30);
		caseA3 = new Case("A",3,0.5);
		caseA4 = new Case("A",4,0.0);
		caseB4 = new Case("B",4,new Addition(caseA1,caseA2));
		caseC4 = new Case("C",4,new Division(caseA1,caseA3));
		caseE4 = new Case("E",4,new Division(caseA2,caseA4)); //Division par 0
		CasesB4_E4.add(caseB4);
		CasesB4_E4.add(caseE4);
		caseD6 = new Case("D",6,new Somme(CasesB4_E4));
		CasesA2_B2_A3.add(caseA2);
		CasesA2_B2_A3.add(caseB2);
		CasesA2_B2_A3.add(caseA3);
		caseB6 = new Case("D",6,new Somme(CasesA2_B2_A3));
		CasesB6_B4_A4.add(caseB6);
		CasesB6_B4_A4.add(caseB4);
		CasesB6_B4_A4.add(caseA4);
		caseC6 = new Case("C",6,new Moyenne(CasesB6_B4_A4));
		caseA6 = new Case("A",6,new Soustraction(caseC6, caseC2));
		initgrille.addCase(caseA1);
		initgrille.addCase(caseA2);
		initgrille.addCase(caseB2);
		initgrille.addCase(caseC2);
		initgrille.addCase(caseA3);
		initgrille.addCase(caseA4);
		initgrille.addCase(caseB4);
		initgrille.addCase(caseC4);
		initgrille.addCase(caseE4); //Division par 0
		initgrille.addCase(caseA6);
		initgrille.addCase(caseB6);
		initgrille.addCase(caseC6);
		initgrille.addCase(caseD6);
	}
	
	@Test 
	public void TestSaveInitiale() throws IOException, ClassNotFoundException, CaseInconnueException{//Sauvegarde de la grille pour l'initialisation dans le client.
		initgrille.save("initGrille.bin");
		initgrille.removeCase(caseE4.getCoord());
		initgrille.load("initGrille.bin");
		Assert.assertEquals(caseE4.getContenu(), initgrille.getCase("E4").getContenu());
	}
}
