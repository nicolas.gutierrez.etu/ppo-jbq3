package TestUnit;

import java.util.LinkedList;

import org.junit.Assert;
import org.junit.Test;

import Noyau.*;

public class TestFormule{
	
	@Test (expected=CycleDetected.class)//procedure de test 1: detectCycle avec cycle
	public void Cycle() throws CycleDetected
	{
		// situation
		Case caseA1 = new Case("A",1,12); //Case dans laquelle on met SOMME(A1;A2)
		Case caseA2 = new Case("A",2,11);
		Case caseA3 = new Case("A",3,10);
		Case caseA4 = new Case("A",4,9);
		Case caseA5 = new Case("A",5,8);
		Case caseA6 = new Case("A",6,0);
		// action
		LinkedList<Case> CasesA1_A5 = new LinkedList<Case>();
		CasesA1_A5.add(caseA1);
		CasesA1_A5.add(caseA2);
		CasesA1_A5.add(caseA3);
		CasesA1_A5.add(caseA4);
		CasesA1_A5.add(caseA5);
		LinkedList<Case> CasesA5_A6 = new LinkedList<Case>();
		CasesA5_A6.add(caseA5);
		CasesA5_A6.add(caseA6);
		LinkedList<Case> CasesA1_A2 = new LinkedList<Case>();
		CasesA1_A2.add(caseA1);
		CasesA1_A2.add(caseA2);
		Somme sommeA1_A2 = new Somme(CasesA1_A2);
		caseA1.setFormule(sommeA1_A2);
	}
	
@Test (expected=CycleDetected.class)//procedure de test 1: detectCycle avec cycle
	public void Cycle2() throws CycleDetected
	{
		// situation
		Case caseA1 = new Case("A",1,12);
		Case caseA2 = new Case("A",2,11);
		Case caseA3 = new Case("A",3,10);
		Case caseA4 = new Case("A",4,9); //Case dans laquelle on mettra SOMME(A5;A6)
		Case caseA5 = new Case("A",5,8);
		Case caseA6 = new Case("A",6,0); //Case dans laquelle on mettra SOMME(A1;A5)
		// action
		LinkedList<Case> CasesA1_A5 = new LinkedList<Case>();
		CasesA1_A5.add(caseA1);
		CasesA1_A5.add(caseA2);
		CasesA1_A5.add(caseA3);
		CasesA1_A5.add(caseA4);
		CasesA1_A5.add(caseA5);
		LinkedList<Case> CasesA5_A6 = new LinkedList<Case>();
		CasesA5_A6.add(caseA5);
		CasesA5_A6.add(caseA6);
		LinkedList<Case> CasesA1_A2 = new LinkedList<Case>();
		CasesA1_A2.add(caseA1);
		CasesA1_A2.add(caseA2);
		Somme sommeA1_A5 = new Somme(CasesA1_A5);
		Somme sommeA5_A6 = new Somme(CasesA5_A6);
		caseA6.setFormule(sommeA1_A5);
		caseA4.setFormule(sommeA5_A6);
	}
	
	@Test//procedure de test 2: detectCycle sans cycle
	public void NoCycle() throws CycleDetected
	{
		// situation
		Case caseA1 = new Case("A",1,12);
		Case caseA2 = new Case("A",2,11);
		Case caseA3 = new Case("A",3,10);
		Case caseA4 = new Case("A",4,9);
		Case caseA5 = new Case("A",5,8);
		Case caseA6 = new Case("A",6,0); //Case dans laquelle on mettra SOMME(A1;A5)
		// action
		LinkedList<Case> CasesA1_A5 = new LinkedList<Case>();
		CasesA1_A5.add(caseA1);
		CasesA1_A5.add(caseA2);
		CasesA1_A5.add(caseA3);
		CasesA1_A5.add(caseA4);
		CasesA1_A5.add(caseA5);
		Somme sommeA1_A5 = new Somme(CasesA1_A5);
		caseA6.setFormule(sommeA1_A5);
		Assert.assertEquals(false,sommeA1_A5.detectCycle(caseA6.getCoord()));
	}
	
	@Test //procedure de test 4: pas de cycle dans moyenne
	public void NoCycle1() throws CycleDetected
	{
		// situation
		Case caseA1 = new Case("A",1,12);
		Case caseA2 = new Case("A",2,11);
		Case caseA3 = new Case("A",3,10);
		Case caseA4 = new Case("A",4,9);
		Case caseA5 = new Case("A",5,8);
		Case caseA6 = new Case("A",6,0); //Case dans laquelle on mettra SOMME(A1;A5)
		// action
		LinkedList<Case> CasesA1_A5 = new LinkedList<Case>();
		CasesA1_A5.add(caseA1);
		CasesA1_A5.add(caseA2);
		CasesA1_A5.add(caseA3);
		CasesA1_A5.add(caseA4);
		CasesA1_A5.add(caseA5);
		Moyenne moyenneA1_A5 = new Moyenne(CasesA1_A5);
		caseA6.setFormule(moyenneA1_A5);
		Assert.assertEquals(false,moyenneA1_A5.detectCycle(caseA6.getCoord()));
	}
}
