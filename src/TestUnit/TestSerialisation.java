package TestUnit;

import Noyau.*;
import java.io.*;

import org.junit.*;

public class TestSerialisation {
	Grille grille;
	Case caseA1;
	Case caseB5;
	Case caseZ2;
	@Before public void setUp() throws CycleDetected{
		grille = new Grille();
		caseA1 = new Case("A",1,12);
		caseB5 = new Case("B",5,24);
		caseZ2 = new Case("Z",2,new Addition(caseA1,caseB5));
		grille.addCase(caseA1);
		grille.addCase(caseB5);
	}
	
	@Test 
	public void TestSave() throws IOException, ClassNotFoundException, CaseInconnueException{
		grille.save("testSave.bin");
		grille.removeCase(caseB5.getCoord());
		grille.load("testSave.bin");
		Assert.assertEquals(caseB5.getContenu(), grille.getCase("B5").getContenu());
	}
	
	@Test (expected=CaseInconnueException.class)
	public void TestSave2() throws IOException, ClassNotFoundException, CaseInconnueException{
		grille.save("testSave.bin");
		grille.addCase(caseZ2);
		grille.load("testSave.bin");
		grille.getCase("Z2");	
	}
	@Test
	public void TestSave3() throws IOException, ClassNotFoundException, CaseInconnueException, CycleDetected{
		Case caseZ3 = new Case("Z",3,new Addition(caseA1,caseB5));//identique à Z2
		grille.addCase(caseZ2);
		grille.save("testSave.bin");
		grille.getCase("Z2").setFormule(new Multiplication(caseA1, caseB5));
		grille.load("testSave.bin");
		Assert.assertEquals(caseZ3.getContenu(), grille.getCase("Z2").getContenu());
	}

}
